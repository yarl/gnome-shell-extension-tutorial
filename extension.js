const St = imports.gi.St;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const Tweener = imports.ui.tweener;
const Panel = Main.panel;
const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const LookingGlass = imports.ui.lookingGlass;

let text, button, lg, sig, osig;

function hideText() {
	if(!text) {
		return;
	}
    	Main.uiGroup.remove_actor(text);
    	text = null;
}

function showText(txt) {
    if (text) {
        hideText();
    }
    text = new St.Label({ style_class: 'tutorial-label', text: txt });
    Main.uiGroup.add_actor(text);

    text.opacity = 255;

    let monitor = Main.layoutManager.primaryMonitor;

    text.set_position(monitor.x,
                      monitor.y + Math.floor(monitor.height / 1.3 - text.height / 2));
}

function _openAppsMenu() {
        Main.panel.statusArea['apps-menu'].menu.toggle();
}

function init() {
	Clutter.init(null);
	lg = new LookingGlass.LookingGlass();
}

function sleep(t) {
	let cb;

	Mainloop.timeout_add(t, function() {
			if(cb)
				cb();
			return false;
		});

	return function(callback) {
		cb = callback;
	};
}

function disconnectSig() {
        print("disconnectSig");
	if(sig && osig) {
		osig.disconnect(sig);
	}
	sig = null;
	osig = null;
}

function step(g) {
	try {
		let f;
		f = g.next();
		print("step");
		f(function() {disconnectSig();step(g)});
	} catch (err if err instanceof StopIteration) {
	} catch (err) {
		print("step err: " + err);
	}
}

function run() {
	showText("Bienvenue dans ce tutoriel sur l'utilisation de votre ordinateur.");
	yield sleep(3000);
	showText("Je vais vous montrer comment utiliser cet environnement.\nD'abord, il me faut vous le décrire.");
	yield sleep(4000);
	showText("Je me borne à vous montrer l'essentiel et vous indiquerais\ncomment vous documenter pour aller au delà.");
	yield sleep(5000);
	showText("D'abord un peu de vocabulaire, que l'on soit d'accord.");
	yield sleep(3000);
	showText("Un \"clic\" est une pression non maintenue,\nc'est à dire appuyer puis tout de suite relacher un bouton\nde la souris. De sorte que l'appuie fait CLI et la relache QUE.\n on à bien CLI-QUE ou CLIQUE ou CLIC.");
	yield sleep(9000);
	showText("Un clic droit c'est quand on clic le bouton de droite de la souris.");
	yield sleep(4000);
	showText("Je vous laisse deviner ce qu'est le clic gauche..\n Quand je ne préciserais pas, cela signifie que c'est un clic gauche.");
	yield sleep(5000);
	showText("Ceci (entouré de rouge) est la \"barre supérieure\",\n entrez la souris dedans sans cliquer");
	let topBar = Main.panel.actor;
	addRBE(topBar);
	yield waitEntry(topBar);
	delRBE(topBar);
	showText("Bravo!");
	yield sleep(2000);
	showText("Tout ce qui se trouve en dessous de la \"barre supérieure\"\nlorsqu'aucune application n'est lancée est vide.");
	yield sleep(4000);
	showText("En effet c'est cette zone que les applications vont utiliser.");
	yield sleep(2000);
	showText("Retournons à notre barre supérieure..");
	yield sleep(2000);
	showText("En haut à droite vous avez un accès aux propriétés du système.\nVeuillez cliquer dessus.");
	let panelRight = Main.panel._rightBox.get_children()[3].get_child();
	addRBE(panelRight);
	yield waitClic(panelRight);
	delRBE(panelRight);
        let e = Main.panel.statusArea.aggregateMenu.menu.firstMenuItem.box;
        addRBE(e);
	showText("Bien. Ceci est le contrôle du volume.");
	yield sleep(3000);
        delRBE(e);
	hideText();
}

function addRBE(o) {
	o.add_effect(lg._redBorderEffect);
}

function delRBE(o) {
	o.remove_effect(lg._redBorderEffect);
}

//todo store all RedBorderEffected objects so we can clean up all of them.
function cleanRBE() {}

function _waitSignal(_sig,o) {
	let cb;
	
	sig = o.connect(_sig, function () {
		if (cb)
			cb();
		return false;
	});
	osig = o;
	
	return function(callback) {
		cb = callback;
	};
}

function waitEntry(o) {
	return _waitSignal('enter-event',o);
}

function waitClic(o) {
	return _waitSignal('button-release-event', o);
}

function enable() {
	let g;
	g = run();
	step(g);
}

function disable() {
	hideText();
	disconnectSig();
	cleanRBE();
}
